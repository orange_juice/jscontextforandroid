package com.philgookang.jscontextandroid.libs;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.philgookang.jscontextandroid.R;

public class WebAppInterface {

    // current activity
    Activity mActivity;

    // init function
    public WebAppInterface(Activity c) {
        mActivity = c;
    }

    @JavascriptInterface
    public void jsToast() {
        Toast.makeText(mActivity, "토스트", Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void jsNotification() {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.mActivity)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("온오프믹스")
                .setContentText("알림")
                .setContentIntent(PendingIntent.getActivity(this.mActivity, (int) System.currentTimeMillis(), new Intent(), 0));

        NotificationManager notificationManager = (NotificationManager)this.mActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
}
